﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvvmLightExample.Design;

namespace MvvmLightExample.Model.Tests
{
    [TestClass]
    public class FileDataServiceTests
    {
        [TestMethod]
        public void GetCurrentPathTest()
        {
            IFileDataService dataService = new FileDataService();
            dataService.GetCurrentPath((item, error) => CheckPath(item, error));
        }

        [TestMethod]
        public void GetCurrentPathDesignTest()
        {
            IFileDataService dataService = new DesignFileDataService();
            dataService.GetCurrentPath((item, error) => CheckPathDesign(item, error));
        }

        private void CheckPath(string callbackPath, Exception error)
        {
            CheckPathShared(callbackPath, FileDataService.STARTUP_PATH, error);
        }

        private void CheckPathDesign(string callbackPath, Exception error)
        {
            CheckPathShared(callbackPath, DesignFileDataService.PATH1, error);
        }

        private void CheckPathShared(string currentPath, string originalPath, Exception error)
        {
            if (error != null)
            {
                Assert.IsTrue(false);
            }
            string path = currentPath;

            bool success = !String.IsNullOrEmpty(path);
            success = success && (originalPath == path);

            Assert.IsTrue(success);
        }

        [TestMethod()]
        public void GetFilesTest()
        {
            IFileDataService dataService = new FileDataService();
            dataService.GetFiles(FileDataService.STARTUP_PATH, (item, error) => CheckFileList(item, error));
        }

        [TestMethod()]
        public void GetFilesDesignTest()
        {
            IFileDataService dataService = new DesignFileDataService();
            dataService.GetFiles(DesignFileDataService.PATH1, (item, error) => CheckFileList(item, error));
        }

        private void CheckFileList(List<FileData> fileList, Exception error)
        {
            if (error != null)
            {
                Assert.IsTrue(false);
            }

            bool success = (null != fileList && 0 < fileList.Count);
            success = success && (!String.IsNullOrEmpty(fileList[0].FileName));

            Assert.IsTrue(success);
        }
    }
}
