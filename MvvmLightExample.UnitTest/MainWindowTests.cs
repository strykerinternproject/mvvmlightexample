﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmLightExample;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace MvvmLightExample.Tests
{
    [TestClass()]
    public class MainWindowTests
    {
        [TestMethod()]
        public void MainWindowTest()
        {
            bool success = true;
            try
            {
                MvvmLightExample.MainWindow mainWindow = new MvvmLightExample.MainWindow();
            }
            catch (Exception ex)
            {
                // ex.Message = @"'Provide value on 'System.Windows.StaticResourceExtension' threw an exception.' Line number '11' and line position '9'."
                string message = ex.Message;
                success = String.IsNullOrEmpty(message);
            }
            Assert.IsTrue(success);
        }
    }
}
