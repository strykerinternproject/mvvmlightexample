﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvvmLightExample.Design;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel.Tests
{
    [TestClass()]
    public class FileExplorerViewModelTests
    {
        [TestMethod()]
        public void FileExplorerViewModelTest()
        {
            IFileDataService dataService = new DesignFileDataService();
            FileExplorerViewModel fileExplorerViewModel = new FileExplorerViewModel(dataService);

            FileData file1 = null;
            bool success = !String.IsNullOrEmpty(fileExplorerViewModel.CurrentPath);
            success = success && (DesignFileDataService.PATH1 == fileExplorerViewModel.CurrentPath);
            success = success && (null != fileExplorerViewModel.FileList && 0 < fileExplorerViewModel.FileList.Count);

            try
            {
                file1 = fileExplorerViewModel.FileList[0];
            }
            catch (Exception)
            {
                success = false;
            }

            success = success && (null != file1) && (DesignFileDataService.FILE_NAME1 == file1.FileName);

            Assert.IsTrue(success);
        }

        [TestMethod()]
        public void CleanupTest()
        {
            Assert.IsTrue(true);
        }
    }
}
