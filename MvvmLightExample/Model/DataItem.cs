﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MvvmLightExample.Model
{
    public class DataItem
    {
        public DataItem(string welcomeTitle)
        {
            WelcomeTitle = welcomeTitle;
            CurrentPath = string.Empty;
        }

        public string WelcomeTitle
        {
            get;
            set;
        }

        public string CurrentPath
        {
            get;
            set;
        }
    }
}
