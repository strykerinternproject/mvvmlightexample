﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MvvmLightExample.Model
{
    /// <summary>
    /// FileData
    /// </summary>
    [DataContract]
    public class FileData
    { 
        /// <summary>
        /// FileData
        /// </summary>
        public FileData()
        {
        }

        /// <summary>
        /// FileName
        /// </summary>
        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        /// <summary>
        /// FilePath
        /// </summary>
        [DataMember]
        public string FilePath
        {
            get;
            set;
        }

        /// <summary>
        /// DateModified
        /// </summary>
        [DataMember]
        public DateTime DateModified
        {
            get;
            set;
        }

        /// <summary>
        /// FileSizeBytes
        /// </summary>
        [DataMember]
        public long FileSizeBytes
        {
            get;
            set;
        }
    }
}
