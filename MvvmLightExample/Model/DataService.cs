﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MvvmLightExample.Model
{
    public class DataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }

        public void GetScreenOneTitle(Action<ScreenDetails, Exception> callback)
        {
            ScreenDetails screenDetails = new ScreenDetails();
            screenDetails.ScreenTitle = "Screen One";
            callback(screenDetails, null);
        }

        public void GetScreenTwoTitle(Action<ScreenDetails, Exception> callback)
        {
            ScreenDetails screenDetails = new ScreenDetails();
            screenDetails.ScreenTitle = "Screen Two";
            callback(screenDetails, null);
        }
    }
}