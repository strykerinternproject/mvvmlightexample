﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmLightExample.Model
{
    public class ScreenDetails
    {
        public ScreenDetails()
        {
            ScreenTitle = string.Empty;
        }

        public string ScreenTitle
        {
            get;
            set;
        }
    }
}
