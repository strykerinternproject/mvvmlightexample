﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace MvvmLightExample.Model
{
    public class FileDataService : IFileDataService
    {
        public static readonly string STARTUP_PATH = @"C:\";

        public void GetCurrentPath(Action<string, Exception> callback)
        {
            string currentPath = STARTUP_PATH;
            callback(currentPath, null);
        }

        public void GetFiles(string path, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = GetFilesFromPath(path, out exception);
            callback(files, exception);
        }

        private List<FileData> GetFilesFromPath(string path, out Exception exception)
        {
            exception = null;
            List<FileData> fileList = new List<FileData>();
            try
            {
                if (Directory.Exists(path))
                {
                    string[] fileEntries = Directory.GetFiles(path);
                    foreach (string fileName in fileEntries)
                    {
                        FileInfo fileInfo = new FileInfo(fileName);
                        FileData file1 = new FileData();
                        file1.FileName = fileName;
                        file1.FilePath = path;
                        file1.FileSizeBytes = fileInfo.Length;
                        file1.DateModified = fileInfo.LastWriteTime;
                        fileList.Add(file1);
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            return fileList;
        }

        public void GetJsonFileList(string fileName, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = OpenJsonFileList(fileName, out exception);
            callback(files, exception);
        }

        private List<FileData> OpenJsonFileList(string fileName, out Exception exception)
        {
            exception = null;
            List<FileData> fileList = null;
            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                JsonSerializer jsonDeserializer = new JsonSerializer();
                fileList = (List<FileData>)jsonDeserializer.Deserialize(streamReader, typeof(List<FileData>));
                streamReader.Close();
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            return fileList;
        }

        public void GetEmbeddedJsonFileList(string fileName, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = OpenEmbeddedJsonFileList(fileName, out exception);
            callback(files, exception);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName">not used</param>
        /// <param name="exception"></param>
        /// <returns></returns>
        private List<FileData> OpenEmbeddedJsonFileList(string fileName, out Exception exception)
        {
            exception = null;
            List<FileData> fileList = null;
            try
            {
                // Use this array to check your resource if you have problems loading it.
                //string[] forDebug_ToShowResourceNames = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
                // e.g. [2] = "MvvmLightExample.Design.Mvvm_Light_Example.json"

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "MvvmLightExample.Design.Mvvm_Light_Example.json";

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                    StreamReader streamReader = new StreamReader(stream);
                    JsonSerializer jsonDeserializer = new JsonSerializer();
                    fileList = (List<FileData>)jsonDeserializer.Deserialize(streamReader, typeof(List<FileData>));
                    //streamReader.Close();
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            return fileList;
        }
    }
}