﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MvvmLightExample.Model
{
    public interface IFileDataService
    {
        void GetFiles(string path, Action<List<FileData>, Exception> callback);
        void GetCurrentPath(Action<string, Exception> callback);
        void GetJsonFileList(string fileName, Action<List<FileData>, Exception> callback);
        void GetEmbeddedJsonFileList(string fileName, Action<List<FileData>, Exception> callback);
    }
}
