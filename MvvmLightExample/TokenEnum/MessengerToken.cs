﻿using MvvmLightExample.ViewModel;

namespace MvvmLightExample
{
    /// <summary>
    /// Enum used with <see cref="GalaSoft.MvvmLight.ViewModelBase.MessengerInstance"/> for register/unregister operations. Uniquely identify messages exchanged between ViewModels.
    /// </summary>
    public enum MessengerToken
    {
        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="WelcomeViewModel"/> for testing messages between two ViewModels.
        /// </summary>
        DisplayMessage,

        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="WelcomeViewModel"/> to test swapping View/ViewModel (<see cref="System.Windows.Controls.UserControl"/>) on the UI.
        /// </summary>
        LoadNext,

        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="FileExplorerViewModel"/> to test 'Open File' for JSON file object deserialization.
        /// </summary>
        OpenFileMessage,

        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="FileExplorerViewModel"/> to test 'Save File' for JSON file object serialization.
        /// </summary>
        SaveFileMessage,

        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="FileExplorerViewModel"/> to test 'Open Embedded' for JSON embedded resource object deserialization.
        /// </summary>
        OpenEmbeddedResourceMessage,

        /// <summary>
        /// Message between <see cref="MenuBarViewModel"/> and <see cref="MainWindowViewModel"/> to show a way to use a delegate to resolve the Window object.
        /// </summary>
        ExitApplicationMessage,
    }
}