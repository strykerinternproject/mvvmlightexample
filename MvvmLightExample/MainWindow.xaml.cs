﻿using System.Windows;
using MvvmLightExample.ViewModel;

namespace MvvmLightExample
{
    /// <summary>
    /// Template class provided when we created a new <see cref="GalaSoft.MvvmLight"/> project that contains interaction logic for MainWindow.xaml - best not to do codebehind in this file.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class (i.e. InitializeComponent). Also, wires up the <see cref="ViewModelLocator"/>.<see cref="ViewModelLocator.Cleanup()"/> deligate.
        /// </summary>
        public MainWindow()
        {
            MainViewModel.GetWindow += DoGetWindow;

            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        /// <summary>
        /// DoGetWindow
        /// </summary>
        /// <returns></returns>
        private Window DoGetWindow()
        {
            return this;
        }
    }
}