﻿using GalaSoft.MvvmLight;
using MvvmLightExample.Model;
using System.Windows;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class WelcomeViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }
            set
            {
                if (value != _welcomeTitle)
                {
                    _welcomeTitle = value;
                    RaisePropertyChanged("WelcomeTitle");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ScreenOneViewModel class.
        /// </summary>
        public WelcomeViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }
                    WelcomeTitle = item.WelcomeTitle;
                });

            MessengerInstance.Register<string>(this, MessengerToken.DisplayMessage, messageText => DoDisplayMessageString(messageText));
        }

        /// <summary>
        /// Method to handle the Messenger Action.
        /// </summary>
        /// <param name="path"></param>
        private void DoDisplayMessageString(string messageText)
        {
            string message = "This is the WelcomeViewModel displaying a message sent from a Sibling ViewModel: " + messageText;
            MessageBox.Show(message, "Testing Messenger between Sibling ViewModels");
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up
            MessengerInstance.Unregister<string>(this, MessengerToken.DisplayMessage, messageText => DoDisplayMessageString(messageText));

            base.Cleanup();
        }
    }
}