﻿using System;
using GalaSoft.MvvmLight;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ScreenTwoViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private string _screenTwoTitle = string.Empty;

        /// <summary>
        /// Gets the ScreenTwoTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ScreenTwoTitle
        {
            get
            {
                return _screenTwoTitle;
            }
            set
            {
                if (value != _screenTwoTitle)
                {
                    _screenTwoTitle = value;
                    RaisePropertyChanged("ScreenTwoTitle");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ScreenOneViewModel class.
        /// </summary>
        public ScreenTwoViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetScreenTwoTitle((item, error) => MoveData(item, error));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="error"></param>
        private void MoveData(ScreenDetails screenDetails, Exception error)
        {
            if (error != null)
            {
                // Report error here
                return;
            }
            ScreenTwoTitle = screenDetails.ScreenTitle;
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up

            base.Cleanup();
        }
    }
}