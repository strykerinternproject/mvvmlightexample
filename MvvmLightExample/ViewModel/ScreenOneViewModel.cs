﻿using System;
using GalaSoft.MvvmLight;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ScreenOneViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private string _screenOneTitle = string.Empty;

        /// <summary>
        /// Gets the ScreenOneTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ScreenOneTitle
        {
            get
            {
                return _screenOneTitle;
            }
            set
            {
                if (value != _screenOneTitle)
                {
                    _screenOneTitle = value;
                    RaisePropertyChanged("ScreenOneTitle");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ScreenOneViewModel class.
        /// </summary>
        public ScreenOneViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetScreenOneTitle((item, error) => MoveData(item, error));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="error"></param>
        private void MoveData(ScreenDetails screenDetails, Exception error)
        {
            if (error != null)
            {
                // Report error here
                return;
            }
            ScreenOneTitle = screenDetails.ScreenTitle;
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up

            base.Cleanup();
        }
    }
}