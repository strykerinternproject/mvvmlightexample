﻿using System;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Delegate type used to get the Main Window
        /// </summary>
        /// <returns>Main Window Object</returns>
        public delegate Window GetViewWindow();

        /// <summary>
        /// Delegate instance to get the Main Window
        /// </summary>
        public static GetViewWindow GetWindow;

        private ViewModelBase _currentViewModel = null;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            _currentViewModel = SimpleIoc.Default.GetInstance<ScreenOneViewModel>();

            MessengerInstance.Register<string>(this, MessengerToken.LoadNext, (text) => LoadNextView(text));
            MessengerInstance.Register<int>(this, MessengerToken.ExitApplicationMessage, (notused) => ExitApplication(notused));
        }

        /// <summary>
        /// This won't work without the DataTemplate in App.xaml or other .xmal location.
        /// </summary>
        public ViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel != value)
                {
                    _currentViewModel = value;
                    RaisePropertyChanged("CurrentViewModel");
                }
            }
        }

        /// <summary>
        /// Handler to dynamically change the view.
        /// </summary>
        /// <param name="text">No used.</param>
        private void LoadNextView(string text)
        {
            ViewModelBase screenTwo = SimpleIoc.Default.GetInstance<ScreenTwoViewModel>();
            ViewModelBase screenOne = SimpleIoc.Default.GetInstance<ScreenOneViewModel>();
            if (CurrentViewModel == screenOne)
            {
                CurrentViewModel = screenTwo;
            }
            else
            {
                CurrentViewModel = screenOne;
            }
        }

        /// <summary>
        /// ExitApplication
        /// </summary>
        /// <param name="notused"></param>
        private void ExitApplication(int notused)
        {
            // If this gets called from a thread other than the UI thread - put it back on the UI thread.
            if (System.Windows.Application.Current.Dispatcher.CheckAccess())
            {
                try
                {
                    if (null != GetWindow)
                    {
                        Window window = GetWindow();
                        if (null != window)
                        {
                            window.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
            else
            {
                // Marshal onto the UI thread.
                System.Windows.Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Send,
                (Action)(() => ExitApplication(notused)));
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up if needed
            MessengerInstance.Unregister<string>(this, MessengerToken.LoadNext, (text) => LoadNextView(text));
            MessengerInstance.Unregister<int>(this, MessengerToken.ExitApplicationMessage, (notused) => ExitApplication(notused));

            //Messenger
            base.Cleanup();
        }
    }
}