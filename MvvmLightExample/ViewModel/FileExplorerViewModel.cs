﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class FileExplorerViewModel : ViewModelBase
    {
        private readonly IFileDataService _fileDataService;
        private ObservableCollection<FileData> _fileList = null;
        private string _currentPath = string.Empty;

        /// <summary>
        /// Gets the FileList property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<FileData> FileList
        {
            get
            {
                return _fileList;
            }
            set
            {
                if (value != _fileList)
                {
                    _fileList = value;
                    RaisePropertyChanged("FileList");
                }
            }
        }

        /// <summary>
        /// Gets the FileList property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string CurrentPath
        {
            get
            {
                return _currentPath;
            }
            set
            {
                if (value != _currentPath)
                {
                    _currentPath = value;
                    RaisePropertyChanged("CurrentPath");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ScreenOneViewModel class.
        /// </summary>
        public FileExplorerViewModel(IFileDataService fileDataService)
        {
            _fileDataService = fileDataService;
            _fileDataService.GetCurrentPath((item, error) => SetPath(item, error));
            _fileDataService.GetFiles(_currentPath, (item, error) => SetFileList(item, error));

            MessengerInstance.Register<string>(this, MessengerToken.OpenFileMessage, messageText => DoOpenFile(messageText));
            MessengerInstance.Register<string>(this, MessengerToken.SaveFileMessage, messageText => DoSaveFile(messageText));
            MessengerInstance.Register<string>(this, MessengerToken.OpenEmbeddedResourceMessage, messageText => DoOpenEmbeddedResource(messageText));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="error"></param>
        private void SetFileList(List<FileData> fileList, Exception error)
        {
            if (error != null)
            {
                System.Windows.MessageBox.Show(error.Message);
                return;
            }
            FileList = new ObservableCollection<FileData>(fileList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="error"></param>
        private void SetPath(string currentPath, Exception error)
        {
            if (error != null)
            {
                System.Windows.MessageBox.Show(error.Message);
                return;
            }
            CurrentPath = currentPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageText">not used</param>
        private void DoOpenFile(string messageText)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.FileName = @"MvvmLightExample.json"; // Default file name
                openFileDialog.DefaultExt = @".json"; // Default file extension
                openFileDialog.Filter = @"JSON documents (.json)|*.json"; // Filter files by extension

                //Show open file dialog box
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (!String.IsNullOrEmpty(openFileDialog.FileName))
                    {
                        CurrentPath = @"File list read from: " + openFileDialog.FileName;
                        _fileDataService.GetJsonFileList(openFileDialog.FileName, (fileList, error) => SetJsonFileList(fileList, error));
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void SetJsonFileList(List<FileData> fileList, Exception error)
        {
            if (error != null)
            {
                System.Windows.MessageBox.Show(error.Message);
                return;
            }
            if (null != fileList)
            {
                FileList = new ObservableCollection<FileData>(fileList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageText">not used</param>
        private void DoSaveFile(string messageText)
        {
            try
            {
                List<FileData> fileList = new List<FileData>(FileList);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = "MvvmLightExample.json"; // Default file name 
                saveFileDialog.DefaultExt = ".json"; // Default file extension 
                saveFileDialog.Filter = "JSON documents (.json)|*.json"; // Filter files by extension

                //Show open file dialog box 
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter myWriter = new StreamWriter(saveFileDialog.FileName);
                    JsonSerializer jsonSerializer = new JsonSerializer();
                    jsonSerializer.Formatting = Newtonsoft.Json.Formatting.Indented;
                    jsonSerializer.Serialize(myWriter, fileList);
                    myWriter.Close();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void DoOpenEmbeddedResource(string messageText)
        {
            try
            {
                CurrentPath = @"File list read from: EmbeddedResource";
                _fileDataService.GetEmbeddedJsonFileList("not used", (fileList, error) => SetJsonFileList(fileList, error));
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up
            MessengerInstance.Unregister<string>(this, MessengerToken.SaveFileMessage, messageText => DoSaveFile(messageText));
            MessengerInstance.Unregister<string>(this, MessengerToken.SaveFileMessage, messageText => DoOpenFile(messageText));
            MessengerInstance.Unregister<string>(this, MessengerToken.OpenEmbeddedResourceMessage, messageText => DoOpenEmbeddedResource(messageText));

            base.Cleanup();
        }
    }
}