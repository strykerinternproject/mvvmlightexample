﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:MvvmLightExample.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using MvvmLightExample.Model;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
                SimpleIoc.Default.Register<IFileDataService, Design.DesignFileDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
                SimpleIoc.Default.Register<IFileDataService, FileDataService>();
            }

            SimpleIoc.Default.Register<MenuBarViewModel>();
            SimpleIoc.Default.Register<WelcomeViewModel>();
            SimpleIoc.Default.Register<ScreenOneViewModel>();
            SimpleIoc.Default.Register<ScreenTwoViewModel>();
            SimpleIoc.Default.Register<FileExplorerViewModel>();
            SimpleIoc.Default.Register<MainViewModel>();
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        /// <summary>
        /// Gets the TopMenuBar property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MenuBarViewModel TopMenuBar
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MenuBarViewModel>();
            }
        }

        /// <summary>
        /// Gets the Welcome property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public WelcomeViewModel Welcome
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WelcomeViewModel>();
            }
        }

        /// <summary>
        /// Gets the ScreenOne property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ScreenOneViewModel ScreenOne
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ScreenOneViewModel>();
            }
        }

        /// <summary>
        /// Gets the ScreenTwo property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ScreenTwoViewModel ScreenTwo
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ScreenTwoViewModel>();
            }
        }

        /// <summary>
        /// Gets the ScreenTwo property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public FileExplorerViewModel FileExplorer
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FileExplorerViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
            SimpleIoc.Default.Unregister<MenuBarViewModel>();
            SimpleIoc.Default.Unregister<WelcomeViewModel>();
            SimpleIoc.Default.Unregister<ScreenOneViewModel>();
            SimpleIoc.Default.Unregister<ScreenTwoViewModel>();
            SimpleIoc.Default.Unregister<FileExplorerViewModel>();
            SimpleIoc.Default.Unregister<MainViewModel>();
        }
    }
}