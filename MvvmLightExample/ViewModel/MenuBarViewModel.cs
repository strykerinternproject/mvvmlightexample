﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace MvvmLightExample.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MenuBarViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MenuBarViewModel class.
        /// </summary>
        public MenuBarViewModel()
        {
        }

        /// <summary>
        /// TestICommand
        /// </summary>
        public ICommand TestICommand
        {
            get
            {
                return new RelayCommand(() => this.DoTestICommand());
            }
        }

        /// <summary>
        /// OpenFile
        /// </summary>
        public ICommand OpenFile
        {
            get
            {
                return new RelayCommand(() => this.DoOpenFile());
            }
        }

        /// <summary>
        /// SaveFile
        /// </summary>
        public ICommand SaveFile
        {
            get
            {
                return new RelayCommand(() => this.DoSaveFile());
            }
        }

        /// <summary>
        /// OpenEmbeddedResource
        /// </summary>
        public ICommand OpenEmbeddedResource
        {
            get
            {
                return new RelayCommand(() => this.DoOpenEmbeddedResource());
            }
        }

        private void DoTestICommand()
        {
            MessageBox.Show("Success. RelayCommand::TestICommand::DoTestICommand", "Testing RelayCommand");
        }

        private void DoOpenFile()
        {
            MessengerInstance.Send("Open File", MessengerToken.OpenFileMessage);
        }

        private void DoSaveFile()
        {
            MessengerInstance.Send("Save File", MessengerToken.SaveFileMessage);
        }

        private void DoOpenEmbeddedResource()
        {
            MessengerInstance.Send("Open Embedded Resource", MessengerToken.OpenEmbeddedResourceMessage);
        }

        /// <summary>
        /// DisplayMessageBox
        /// </summary>
        public ICommand DisplayMessageBox
        {
            get
            {
                return new RelayCommand(() => this.DisplayMessageStringCommand());
            }
        }

        private void DisplayMessageStringCommand()
        {
            MessengerInstance.Send("Message from MenuBarViewModel.", MessengerToken.DisplayMessage);
        }

        /// <summary>
        /// LoadNextView
        /// </summary>
        public ICommand LoadNextView
        {
            get
            {
                return new RelayCommand(() => this.LoadNextViewCommand());
            }
        }

        /// <summary>
        /// ExitApp
        /// </summary>
        public ICommand ExitApp
        {
            get
            {
                return new RelayCommand(() => this.DoExitAppCommand());
            }
        }

        private void DoExitAppCommand()
        {
            MessengerInstance.Send(0, MessengerToken.ExitApplicationMessage);
        }

        private void LoadNextViewCommand()
        {
            MessengerInstance.Send("Hi, this is the next View.", MessengerToken.LoadNext);
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public override void Cleanup()
        {
            // Clean up
            
            base.Cleanup();
        }
    }
}