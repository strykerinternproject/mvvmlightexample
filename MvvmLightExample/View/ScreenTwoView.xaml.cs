﻿using System.Windows.Controls;

namespace MvvmLightExample.View
{
    /// <summary>
    /// Description for ScreenOneView.
    /// </summary>
    public partial class ScreenTwoView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the ScreenOneView class.
        /// </summary>
        public ScreenTwoView()
        {
            InitializeComponent();
        }
    }
}