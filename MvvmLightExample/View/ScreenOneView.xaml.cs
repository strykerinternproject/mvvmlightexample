﻿using System.Windows.Controls;

namespace MvvmLightExample.View
{
    /// <summary>
    /// Description for ScreenOneView.
    /// </summary>
    public partial class ScreenOneView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the ScreenOneView class.
        /// </summary>
        public ScreenOneView()
        {
            InitializeComponent();
        }
    }
}