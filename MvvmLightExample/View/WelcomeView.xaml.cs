﻿using System.Windows.Controls;

namespace MvvmLightExample.View
{
    /// <summary>
    /// Description for WelcomeView.
    /// </summary>
    public partial class WelcomeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the WelcomeView class.
        /// </summary>
        public WelcomeView()
        {
            InitializeComponent();
        }
    }
}