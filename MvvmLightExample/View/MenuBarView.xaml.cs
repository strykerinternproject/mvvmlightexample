﻿using System.Windows;
using System.Windows.Controls;

namespace MvvmLightExample.View
{
    /// <summary>
    /// Description for MenuBarView.
    /// </summary>
    public partial class MenuBarView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the MenuBarView class.
        /// </summary>
        public MenuBarView()
        {
            InitializeComponent();
        }
    }
}