﻿using System.Windows.Controls;

namespace MvvmLightExample.View
{
    /// <summary>
    /// Description for FileExplorerView.
    /// </summary>
    public partial class FileExplorerView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the FileExplorerView class.
        /// </summary>
        public FileExplorerView()
        {
            InitializeComponent();
        }
    }
}