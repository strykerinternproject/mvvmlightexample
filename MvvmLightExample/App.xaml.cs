﻿using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace MvvmLightExample
{
    /// <summary>
    /// Template class provided when we created a new <see cref="GalaSoft.MvvmLight"/> project that contains interaction logic for App.xaml - best not to do codebehind in this file.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the App class. Also, <see cref="GalaSoft.MvvmLight.Threading.DispatcherHelper"/> Initialized.
        /// NOTE: It appears that for some reason this documentation comment does not show up with SHFB.
        /// </summary>
        static App()
        {
            DispatcherHelper.Initialize();
        }
    }
}
