﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using MvvmLightExample.Model;
using Newtonsoft.Json;

namespace MvvmLightExample.Design
{
    /// <summary>
    /// DesignFileDataService
    /// </summary>
    public class DesignFileDataService : IFileDataService
    {
        /// <summary>
        /// FILE_NAME1
        /// </summary>
        public static readonly string FILE_NAME1 = @"App.config [design]";
        
        /// <summary>
        /// PATH1
        /// </summary>
        public static readonly string PATH1 = @"C:\ [design]";

        /// <summary>
        /// GetCurrentPath
        /// </summary>
        /// <param name="callback">callback</param>
        public void GetCurrentPath(Action<string, Exception> callback)
        {
            string currentPath = PATH1;
            callback(currentPath, null);
        }

        /// <summary>
        /// GetFiles
        /// </summary>
        /// <param name="path">path</param>
        /// <param name="callback">callback</param>
        public void GetFiles(string path, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = GetDesignFileList(out exception);
            callback(files, exception);
        }

        /// <summary>
        /// GetDesignFileList
        /// </summary>
        /// <param name="exception">exception</param>
        /// <returns><see cref="List"/> of <see cref="FileData"/></returns>
        private List<FileData> GetDesignFileList(out Exception exception)
        {
            exception = null;

            // Create design time data.
            //List<FileData> fileList = new List<FileData>();
            //FileData file1 = new FileData();
            //file1.FileName = FILE_NAME1;
            //file1.FilePath = @"D:\Dev\ [design]";
            //file1.FileSizeBytes = 5120; // 5KB
            //file1.DateModified = DateTime.Now;
            //fileList.Add(file1);
            List<FileData> fileList = OpenEmbeddedJsonFileList("not used", out exception);

            return fileList;
        }

        /// <summary>
        /// GetJsonFileList
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <param name="callback">callback</param>
        public void GetJsonFileList(string fileName, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = OpenEmbeddedJsonFileList(fileName, out exception);
            callback(files, exception);
        }

        /// <summary>
        /// GetEmbeddedJsonFileList
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <param name="callback">fileName</param>
        public void GetEmbeddedJsonFileList(string fileName, Action<List<FileData>, Exception> callback)
        {
            Exception exception;
            List<FileData> files = OpenEmbeddedJsonFileList(fileName, out exception);
            callback(files, exception);
        }
        
        /// <summary>
        /// OpenEmbeddedJsonFileList
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <param name="exception">exception</param>
        /// <returns><see cref="List"/> of <see cref="FileData"/></returns>
        private List<FileData> OpenEmbeddedJsonFileList(string fileName, out Exception exception)
        {
            exception = null;
            List<FileData> fileList = null;

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "MvvmLightExample.Design.Mvvm_Light_Example.json";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                StreamReader streamReader = new StreamReader(stream);
                JsonSerializer jsonDeserializer = new JsonSerializer();
                fileList = (List<FileData>)jsonDeserializer.Deserialize(streamReader, typeof(List<FileData>));
                //streamReader.Close();
            }
            return fileList;
        }

    }
}