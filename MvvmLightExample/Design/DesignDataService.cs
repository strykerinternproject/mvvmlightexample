﻿using System;
using System.Collections.Generic;
using MvvmLightExample.Model;

namespace MvvmLightExample.Design
{
    /// <summary>
    /// DesignDataService
    /// </summary>
    public class DesignDataService : IDataService
    {
        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="callback">callback</param>
        public void GetData(Action<DataItem, Exception> callback)
        {
            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }

        /// <summary>
        /// GetScreenOneTitle
        /// </summary>
        /// <param name="callback">callback</param>
        public void GetScreenOneTitle(Action<ScreenDetails, Exception> callback)
        {
            ScreenDetails screenDetails = new ScreenDetails();
            screenDetails.ScreenTitle = "Screen One [design]";
            callback(screenDetails, null);
        }

        /// <summary>
        /// GetScreenTwoTitle
        /// </summary>
        /// <param name="callback">callback</param>
        public void GetScreenTwoTitle(Action<ScreenDetails, Exception> callback)
        {
            ScreenDetails screenDetails = new ScreenDetails();
            screenDetails.ScreenTitle = "Screen Two [design]";
            callback(screenDetails, null);
        }
    }
}